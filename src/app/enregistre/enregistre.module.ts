import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { IonicModule } from '@ionic/angular';

import { EnregistrePage } from './enregistre.page';

const routes: Routes = [
  {
    path: '',
    component: EnregistrePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AngularFirestoreModule,
    RouterModule.forChild(routes),
    AngularFireAuthModule
  ],
  declarations: [EnregistrePage]
})
export class EnregistrePageModule {}
