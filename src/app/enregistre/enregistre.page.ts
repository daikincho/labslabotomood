import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { firebase } from '@firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserLabo } from '../../models/userLabo';
import { Storage } from '@ionic/storage';




import '@firebase/auth';
import '@firebase/database';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enregistre',
  templateUrl: './enregistre.page.html',
  styleUrls: ['./enregistre.page.scss'],
})
export class EnregistrePage implements OnInit {

  user : firebase.User;
  userDocCollection:any;
  myProfil:any;

  currentUser = new UserLabo();

  email: string;
  password: string;
  name: string;
  confirmation_password: string;

  constructor(private _toastCtrl: ToastController,
     private _auth: AngularFireAuth,
     private _database : AngularFirestore,
     private router:Router,
     private storage: Storage) {  
     
    this.userDocCollection = _database.collection<any>('users');
  }

  ngOnInit() {
  }

  signupUser(): Promise<any> {
    if (this.currentUser.password === this.confirmation_password
      && this.currentUser.roles.length > 0
      && this.currentUser.roles.length < 3) {
      return firebase
      .auth()
      .createUserWithEmailAndPassword(this.currentUser.email, this.currentUser.password)
      .then(newUserCredential => {
      })
      .then( async _ => {
        (await this._toastCtrl.create({
          message: 'you are now created !',
          color: 'success',
          duration: 3000
        })).present();
      }).then( success => {

        this.CreateUserInDataBase();
        this.router.navigate(['/tabs']);


      })
      .catch(async err => {
        console.log(err);
        (await this._toastCtrl.create({
          message: 'error: ' + err,
          color: 'danger',
          duration: 2000
        })).present();
      });
    }

    this.presentToast('you are not created ! Either you have more than 3 roles, 0 role or your passwords don\'t match', 'danger');
    return null;
  }

  async presentToast(message: string, color: string) {
    const toast = await this._toastCtrl.create({
      message: message,
      duration: 2000,
      color : color
    });
    toast.present();
  }

  CreateUserInDataBase(){

      this.myProfil = {
      id:"",
      name: this.currentUser.name,
      email: this.currentUser.email,
      roles: this.currentUser.roles,
      password: this.currentUser.password,
      poles: this.currentUser.poles,
      Project_ref: []
    }
    
    this.userDocCollection.add(this.myProfil).then(docRef => {
      this.myProfil.id = docRef.id;
      let myDoc = this._database.doc<any>(`users/${this.myProfil.id}`); 
      myDoc.update(this.myProfil);
      this.storage.set('UserConnected', this.myProfil);


    });



    /*this._database.collection<userLabo>('users').valueChanges().subscribe(data=>{
      this.userDoc = this._database.doc<any>('users/7rZqq8DZOvjWDDThS136');

      this.userDoc.set({
        name: this.currentUser.name,
        email: this.currentUser.email,
        // Other info you want to add here
      })*/
  }

}
