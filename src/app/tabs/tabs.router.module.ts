import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: '../tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: '../tab2/tab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'project',
        children: [
          {
            path: '',
            loadChildren: '../project/project.module#ProjectPageModule'
          }
        ]
      },
      {
        path: 'task-tracking',
        children: [
          {
            path: '',
            loadChildren: '../task-tracking/task-tracking.module#TaskTrackingPageModule'
          }
        ]
      },
      {
        path: 'task',
        children: [
          {
            path: '',
            loadChildren: '../task/task.module#TaskPageModule'
          }
        ]
      },
      {
        path: 'pole',
        children: [
          {
            path: '',
            loadChildren: '../pole/pole.module#PolePageModule'
          }
        ]
      },
      {
        path: 'opinion',
        children: [
          {
            path: '',
            loadChildren: '../opinion/opinion.module#OpinionPageModule'
          }
        ]
      },
      {
        path: 'setting',
        children: [
          {
            path: '',
            loadChildren: '../setting/setting.module#SettingPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
