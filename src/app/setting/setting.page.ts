import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {UserLabo} from '../../models/userLabo';


@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  alertDate;
  currentUser = new UserLabo();
  public date: Date
  constructor(private router: Router,
    private storage: Storage,
  ) { }

  ngOnInit() {}

  goToDeconnexion() {
    this.router.navigate(['/launch']);
    this.storage.remove('UserConnected').then((user) => {
      this.currentUser = user;
      console.log('User disconnected');
    });
  }

  alertNotification() {
    let alertDate = this.alertDate;
    console.log(alertDate);

    this.storage.get('DateAlert').then((date) => {
      alertDate = date;
      console.log('Storage : ' + alertDate);
    });
  }

}
