import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, AlertController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-launch',
  templateUrl: './launch.page.html',
  styleUrls: ['./launch.page.scss'],
})
export class LaunchPage implements OnInit {
  
  constructor(private router: Router, private plt: Platform, private localNotifications: LocalNotifications, private alertCtrl: AlertController) {
    this.initPlatform();
  }

  ngOnInit() {
  }

  goToConnexion() {
    this.router.navigate(['/connexion']);
  }

  goToEnregistre() {
    this.router.navigate(['/enregistre']);
  }

  private initPlatform() {
    this.plt.ready().then(() => {
      this.localNotifications.on('click').subscribe(res => {
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      });
 
      this.localNotifications.on('trigger').subscribe(res => {
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      });
      this.scheduleNotification();
    });
  }

  public scheduleNotification() {
    let date = new Date();
    this.localNotifications.schedule({
      id: 1,
      title: 'Session terminée !',
      text: 'Donnez votre avis',
      data: { mydata: 'Donnez votre avis' },
      trigger: { every: { hour: 18, minute: 0}, count: 1 },
      foreground: true
    });
  }
 
  private showAlert(header: string, sub: string, msg: string) {
    this.alertCtrl.create({
      header: header,
      subHeader: sub,
      message: msg,
      buttons: ['Ok']
    }).then(alert => alert.present());
  }
}
