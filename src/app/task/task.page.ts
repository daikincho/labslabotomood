import { Component, OnInit } from '@angular/core';
import { IPole } from '../entities/Pole/Pole.model';
import { PoleService } from '../entities/Pole/Pole.service';
import { Storage } from '@ionic/storage';
import { TasksFirebaseService } from '../../Services/TasksFirebaseService';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  tasksDocCollection:any;
  projectDocCollection:any;
  allProjects : any[] = [];
  allTasks : any[] = [];
  TaskTitle :string;
  TaskDescription:string;
  TaskProject:any;
  

  constructor(
    private storage: Storage,
    private tasksFirebaseService : TasksFirebaseService) { }

  ngOnInit() {

    this.tasksFirebaseService.InitiliseDatabase(this);


  }


  saveTask(){
    this.tasksFirebaseService.saveNewTask(this);
  }


  onProjectChanged(newValue){

    this.TaskProject = this.allProjects.find(p => p.name == newValue);
    


  }

}
