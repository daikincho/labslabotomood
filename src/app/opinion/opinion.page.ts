import { Component, OnInit } from '@angular/core';
import { Opinion, IOpinion } from '../entities/Opinion/Opinion.model';
import { OpinionService } from '../entities/Opinion/Opinion.service';
import { ActivatedRoute } from '@angular/router';
import { ISession } from '../entities/Session/Session.model';
import { SessionService } from '../entities/Session/Session.service';
import { Storage } from '@ionic/storage';
import { UserLabo } from '../../models/userLabo';
import { Location } from '@angular/common';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.page.html',
  styleUrls: ['./opinion.page.scss'],
})
export class OpinionPage implements OnInit {

  public errorOccured: boolean = false;
  public opinion: IOpinion;
  public sessions: ISession[] = [];
  public currentUser: UserLabo;
  public error = "";

  constructor(private activateRoute: ActivatedRoute,
    private opinionService: OpinionService,
    private sessionService: SessionService,
    private storage: Storage,
    private _location: Location,
    private alertCtrl: AlertController
  ) {
      // get user connected
      this.storage.get('UserConnected').then((user) => {
        // get all sessions where the date is lower or equal to today
        this.sessionService.getSessions().subscribe(
          sessions => {
            // get all opinions of the user
            this.opinionService.getUserOpinions(user.email).subscribe(
              opinions => {
                sessions.forEach(session => {
                  // fills this.sessions with the sessions where the user didn't commented yet
                  if (!opinions.find(function(opinion) { return opinion.sessionRef == session.ref })) {
                    this.sessions.push(session);
                  }
                })
                this.considerSessions();
              },
              error => {
                // error during extraction of user opinions, doesn't stop opinion form process
                console.log(error);
                this.sessions = sessions;
                this.considerSessions();
                this.error = error;
              }
            )
          },
          error => {
            // error during extraction of sessions stops opinion form process as you can't submit with an empty field
            this.errorOccured = true;
            this.showAlert("Erreur", "Une erreur est survenu lors de la lecture des sessions", () => { this.previousPage() });
            console.log(error);
          }
        )
        this.opinion = new Opinion(user.email);
      }).catch(error => {
        console.log(error);
        this.errorOccured = true;
        this.error = error;
      });
  }

  ngOnInit() {
  }
  
  // check this.sessions list and show an alert if it is empty
  private considerSessions() {
    if (this.sessions.length < 1) {
      this.showAlert("Erreur", "Vous avez déjà donné votre avis à toutes les sessions", () => { this.previousPage() });
    }
  }

  public submitOpinion() {
    try {
      this.opinionService.save(this.opinion).then(opinionAdded => {
        this.showAlert("Merci", "Votre opinion a été enregistrée avec succès", () => { this.previousPage() });
      });
    } catch (error) {
      console.log(error);
      this.showAlert("Erreur", "Une erreur est survenu, vous n'avez surement pas rempli tous les champs", () => { });
    }
  }
 
  private showAlert(header: string, message: string, onDidDismiss: () => void) {
    this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok']
    }).then(alert => {
      alert.onDidDismiss().then(() => {
        onDidDismiss();
      });
      alert.present();
    });
  }

  public previousPage() {
    this._location.back();
  }
}
