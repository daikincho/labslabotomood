import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolePage } from './pole.page';

describe('PolePage', () => {
  let component: PolePage;
  let fixture: ComponentFixture<PolePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PolePage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
