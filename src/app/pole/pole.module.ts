import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PolePage } from './pole.page';
import { AngularFireStorageModule } from '@angular/fire/storage';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: PolePage }]),
    AngularFireStorageModule
  ],
  declarations: [PolePage]
})
export class PolePageModule {}
