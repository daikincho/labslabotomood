import { Component, OnInit } from '@angular/core';
import { IPole } from '../entities/Pole/Pole.model';
import { PoleService } from '../entities/Pole/Pole.service';
import { PolesFirebaseService } from '../../Services/PolesFirebaseService';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-pole',
  templateUrl: 'pole.page.html',
  styleUrls: ['pole.page.scss']
})
export class PolePage implements OnInit {
  errorOccured: boolean = false;


  polesDocCollection:any;
  projectDocCollection:any;
  allProjects : any[] = [];
  allPoles : any[] = [];




  
  constructor(    
    private storage: Storage,
    private polesFirebaseService : PolesFirebaseService

  ) {
  }

  ngOnInit() {
    /*PoleService.getAll().subscribe(
      (poles) => {
        this.poles = poles;
      },
      (error) => {
        this.errorOccured = true;
      }
    );*/

    this.polesFirebaseService.InitiliseDatabase(this);



    

  }
}
