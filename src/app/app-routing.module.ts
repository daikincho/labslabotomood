import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'launch', loadChildren: './launch/launch.module#LaunchPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'connexion', loadChildren: './connexion/connexion.module#ConnexionPageModule' },
  { path: 'enregistre', loadChildren: './enregistre/enregistre.module#EnregistrePageModule' },
  { path: '', loadChildren: './splash/splash.module#SplashPageModule' },
  { path: 'task', loadChildren: './task/task.module#TaskPageModule' }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
