import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { firebase } from '@firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserLabo } from '../../models/userLabo';
import { Storage } from '@ionic/storage';
import lodash from 'lodash';

import '@firebase/auth';
import '@firebase/database';
import { Router } from '@angular/router';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

  user: firebase.User;
  userDocCollection: any;
  usersRef: any;


  currentUser = new UserLabo();

  constructor(private _toastCtrl: ToastController,
    private _auth: AngularFireAuth,
    private _database: AngularFirestore,
    private router: Router,
    private storage: Storage
  ) {
   // this.userDocCollection = _database.collection<any>('users');

    _database.collection<any>('users').valueChanges().subscribe(data => {
      this.userDocCollection = data;
    });

    }

  ngOnInit() {
  }

  loginUser() {
    this._auth.auth.signInWithEmailAndPassword(this.currentUser.email, this.currentUser.password)
    .then(async _ => {
      (await this._toastCtrl.create({
        message: 'you are now connected !',
        color: 'success',
        duration: 3000
      })).present();
    })
    .then(() => {
      this.storage.ready().then(() => {
        this.currentUser = this.userDocCollection.find(user => user.email === this.currentUser.email);
       // this.currentUser = this.userDocCollection.where("email", "==",this.currentUser.email)

       //transforme le json trop gros en string pour permettre le stockage en local grace a lodash
        let userToSave = lodash.clone(this.currentUser);
        userToSave.Project_ref = userToSave.Project_ref ? userToSave.Project_ref.map(x => x.id) : [];
        this.storage.set('UserConnected', userToSave).then(() => {
          this.router.navigate(['/tabs']);
        });

      }).catch(err => {
        console.log(err);
      });
      
    })
    .catch(async err => {
      (await this._toastCtrl.create({
        message: 'error: ' + err,
        color: 'danger',
        duration: 2000
      })).present();
    });
  }


  async presentToast(message: string, color: string) {
    const toast = await this._toastCtrl.create({
      message: message,
      duration: 2000,
      color : color
    });
    toast.present();
  }

}
