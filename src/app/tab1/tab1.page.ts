import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { firebase } from '@firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserLabo } from '../../models/userLabo';
import { Storage } from '@ionic/storage';


import '@firebase/auth';
import '@firebase/database';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';

enum Status {
  'Head Master',
  'Project Leader',
  'Project Member'
}

enum Poles {
  'Game',
  'Security',
  'Data',
  'Web',
  'Mobile',
  'IOT',
  'Crea'
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})


export class Tab1Page implements OnInit {

  user: firebase.User;
  ProjectDocCollection: any;

  currentUser = new UserLabo();

  constructor(private _toastCtrl: ToastController,
    private _auth: AngularFireAuth,
    private _database: AngularFirestore,
    private router: Router,
    private storage: Storage
  ) {    }

  ngOnInit() {
    this._auth.user.subscribe(user => {
      this.user = user;
    });

    this._database.collection<any>('Project').valueChanges().subscribe(data => {
      this.ProjectDocCollection = data;
    });


    this.storage.get('UserConnected').then((user) => {
      this.currentUser = user;
    }).catch(err => {
      this.presentToast(err.message, "danger");
    });
  }



  async presentToast(message: string, color: string) {
    const toast = await this._toastCtrl.create({
      message: message,
      duration: 2000,
      color : color
    });
    toast.present();
  }
}
