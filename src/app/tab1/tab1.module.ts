import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AngularFirestoreModule,
    IonicStorageModule.forRoot(),
    RouterModule.forChild([{ path: '', component: Tab1Page }]),
    AngularFireAuthModule
  ],
  declarations: [Tab1Page]
})
export class Tab1PageModule {}
