import { Component, OnInit } from '@angular/core';
import { Project, IProject } from '../entities/Project/Project.model';
import { ProjectService } from '../entities/Project/Project.service';
import { ActivatedRoute } from "@angular/router";
import { ITask } from '../entities/Task/Task.model';
import { ITaskManager, TaskManager } from '../entities/TaskManager/TaskManager.model';
import { IUser, User } from '../entities/User/User.model';
import { IPole, Pole } from '../entities/Pole/Pole.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.page.html',
  styleUrls: ['./project.page.scss'],
})
export class ProjectPage implements OnInit {
  project?: IProject;
  pole?: IPole;
  taskManager?: TaskManager;
  users?: IUser[] = [];
  projectNotFound: boolean = true;
  lastTask: ITask = null;
    
  constructor(private activateRoute: ActivatedRoute, private projectService: ProjectService) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params => {
      if (params.get("id") != null) {
        this.buildProjectValues(params.get("id"));
      }
    })
  }

  private buildProjectValues(projectRef: string) {
    
    this.projectService.getProject(projectRef).subscribe(
      (project) => {
        this.projectNotFound = false;
        this.project = project;
        this.buildPole();
        this.buildTaskManager();
        this.buildUsers();
      }
    )
  }

  private buildPole() {
    this.project.poleRef.get().then(poleDoc => {
      const poleData = poleDoc.data();
      this.pole = new Pole(
        poleDoc.id,
        poleData.name,
        poleData.projectRefs
      )
    });
  }

  private buildTaskManager() {
    this.project.taskManagerRef.get().then(taskManagerDoc => {
      const taskManagerData = taskManagerDoc.data();
      this.taskManager = new TaskManager(
        taskManagerDoc.id,
        taskManagerData.name,
        taskManagerData.taskListRefs
      )
    });
  }

  private buildUsers() {
    this.project.userRefs.forEach(userRef => {
      userRef.get().then(userDoc => {
        const userData = userDoc.data();
        this.users.push(new User(
          userData.email,
          userData.name,
          userData.roles,
          userData.projectRef,
          userData.poleRef
        ));
      });
    });
  }
}
