import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskTrackingPage } from './task-tracking.page';

describe('TaskTrackingPage', () => {
  let component: TaskTrackingPage;
  let fixture: ComponentFixture<TaskTrackingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskTrackingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTrackingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
