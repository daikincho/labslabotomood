import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../entities/Project/Project.service';
import { Project } from '../entities/Project/Project.model';

@Component({
  selector: 'app-task-tracking',
  templateUrl: './task-tracking.page.html',
  styleUrls: ['./task-tracking.page.scss'],
})
export class TaskTrackingPage implements OnInit {
  project?: Project;
  projectNotFound = false;

  constructor(private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(params => {/*
      ProjectService.get(+params.get("projectId")).subscribe(
        (project) => {
          this.project = project;
        },
        (error) => {
          this.projectNotFound = true;
        }
      )
      */})
  }

}
