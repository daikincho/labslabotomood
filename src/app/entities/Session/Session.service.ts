import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { ISession } from './Session.model';


@Injectable({ providedIn: 'root' })
export class SessionService {

    constructor(private afs : AngularFirestore) {
    }

    public getSessions() : Observable<ISession[]> {
        return this.afs.collection<ISession>("session", ref => ref.where("date", "<=", new Date())).snapshotChanges().pipe(
            map(sessiongDcumentChangeActions => {
                return sessiongDcumentChangeActions.map(sessiongDcumentChangeAction => {
                    return {
                        "ref": sessiongDcumentChangeAction.payload.doc.id,
                        "date": sessiongDcumentChangeAction.payload.doc.data().date
                    }
                })
            })
        )
    }
}