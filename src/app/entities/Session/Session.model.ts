import { IOpinion } from "../Opinion//Opinion.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface ISession {
    ref?: string;
    date?: Date;
    opinions?: DocumentReference[]
}

export class Session implements ISession {
    constructor (
        public ref: string,
        public date?: Date,
        public opinions?: DocumentReference[]
    ) { }
}
