import { Injectable } from '@angular/core';
import { IPole, Pole } from './Pole.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';


@Injectable({ providedIn: 'root' })
export class PoleService {

    constructor(private afs : AngularFirestore) {
    }

    public getPoles() : Observable<IPole[]> {
        return this.afs.collection<IPole>("pole").snapshotChanges().pipe(
            map((polesDocChangeActions) => {
                return polesDocChangeActions.map((poleDocChangeAction) => {
                    const poleRef = poleDocChangeAction.payload.doc.id;
                    const poleData = poleDocChangeAction.payload.doc.data();
                    return new Pole(
                        poleRef,
                        poleData.name,
                        poleData.projectRefs
                    )
                })
            })
        )
    }

    public getPole(poleRef: string) : Observable<IPole> {
        return this.afs.doc<IPole>("pole/" + poleRef).snapshotChanges().pipe(
            map(poleDocumentSnapshotAction => {
                const poleRef = poleDocumentSnapshotAction.payload.id;
                const poleData = poleDocumentSnapshotAction.payload.data();
                return new Pole(
                    poleRef,
                    poleData.name,
                    poleData.projectRefs
                )
            })
        );
    }
}
