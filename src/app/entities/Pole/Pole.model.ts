import { IProject, Project } from "../Project/Project.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface IPole {
    ref?: string;
    name?: string;
    projectRefs?: DocumentReference[];
}

export class Pole implements IPole {

    constructor (
        public ref?: string,
        public name?: string,
        public projectRefs?: DocumentReference[]
    ) {}
}
