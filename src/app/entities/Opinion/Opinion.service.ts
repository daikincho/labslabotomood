import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { IOpinion, Opinion } from './Opinion.model';


@Injectable({ providedIn: 'root' })
export class OpinionService {

    constructor(private afs : AngularFirestore) {
    }

    public save(opinion: IOpinion) : Promise<DocumentReference> {
        return this.afs.collection("opinion").add({
            "userEmail": opinion.userEmail,
            "sessionRef": opinion.sessionRef,
            "accomplishedTasks": opinion.accomplishedTasks,
            "difficultiesMet": opinion.difficultiesMet,
            "mark": opinion.mark
        })
    }

    public getUserOpinions(userMail: string) : Observable<IOpinion[]> {
        return this.afs.collection<IOpinion>("opinion", ref => ref.where("userEmail", "==", userMail)).snapshotChanges().pipe(
            map(opinionDocumentChangeActions => {
                return opinionDocumentChangeActions.map(opinionDocumentChangeAction => {
                    const opinionData = opinionDocumentChangeAction.payload.doc.data();
                    return new Opinion (
                        opinionData.userEmail,
                        opinionData.sessionRef
                    )
                })
            })
        )
    }
}