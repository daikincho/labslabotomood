import { DocumentReference } from "@angular/fire/firestore";

export interface IOpinion {
    userEmail?: string;
    sessionRef?: string;
    accomplishedTasks?: string;
    difficultiesMet?: string;
    mark?: number;
}

export class Opinion implements IOpinion {
    constructor (
        public userEmail?: string,
        public sessionRef?: string,
        public accomplishedTasks?: string,
        public difficultiesMet?: string,
        public mark?: number,
    ) { }
}
