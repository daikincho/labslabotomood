import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { IUser, User } from './User.model';


@Injectable({ providedIn: 'root' })
export class UserService {

    constructor(private afs : AngularFirestore) {
    }
    
    public GetUserCurrentlyConnected() : Observable<IUser> {
        return this.afs.doc<IUser>("users/7EexsBX7xFjE2F6noJqJ").snapshotChanges().pipe(
            map(userDocumentSnapshotAction => {
                const userData = userDocumentSnapshotAction.payload.data();
                return new User (
                    userData.email,
                    userData.name
                )
            })
        );
    }
}