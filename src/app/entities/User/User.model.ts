import { IProject } from "../Project/Project.model";
import { IPole } from "../Pole/Pole.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface IUser {
    email?: string;
    name?: string;
    roles?: string[];
    projectRef?: DocumentReference;
    poleRef?: DocumentReference;
}

export class User implements IUser{
    constructor (
        public email?: string,
        public name?: string,
        public roles?: string[],
        public projectRef?: DocumentReference,
        public poleRef?: DocumentReference
    ) { }
}
