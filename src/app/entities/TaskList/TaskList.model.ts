import { ITask, Task } from "../Task/Task.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface ITaskList {
    name?: string;
    taskRefs?: DocumentReference[];
}

export class TaskList {

    public tasks: Task[] = [];

    constructor (
        public name?: string,
        public taskRefs?: DocumentReference[]
    ) { 
        this.initTasks();
    }

    private initTasks() {
        for (let taskRef of this.taskRefs) {
            taskRef.get().then((taskDoc) => {
                const taskData = taskDoc.data()
                this.tasks.push(new Task(
                    taskDoc.id,
                    taskData.title,
                    taskData.description,
                    taskData.date.toDate(),
                    taskData.taskStateRef
                ));
            })
        }
    }
}
