import { IUser } from "../User/User.model";
import { IPole, Pole } from "../Pole/Pole.model";
import { ITaskManager, TaskManager } from "../TaskManager/TaskManager.model";
import { ITask } from "../Task/Task.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface IProject {
    ref?: string;
    name?: string;
    taskManagerRef?: DocumentReference;
    poleRef?: DocumentReference;
    userRefs?: DocumentReference[];
}

export class Project implements IProject {

    constructor (
        public ref?: string,
        public name?: string,
        public taskManagerRef?: DocumentReference,
        public poleRef?: DocumentReference,
        public userRefs?: DocumentReference[]
    ) { }
}
