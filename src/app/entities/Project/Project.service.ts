import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { IProject, Project } from './Project.model';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProjectService {

    constructor(private afs : AngularFirestore) {
    }

    public getProjects() : Observable<IProject[]> {
        return this.afs.collection<IProject>("project").snapshotChanges().pipe(
            map((projectDocChangeActions) => {
                return projectDocChangeActions.map((projectDocChangeAction) => {
                    const projectRef = projectDocChangeAction.payload.doc.id;
                    const projectData = projectDocChangeAction.payload.doc.data();
                    return new Project(
                        projectRef,
                        projectData.name,
                        projectData.taskManagerRef,
                        projectData.poleRef,
                        projectData.userRefs
                    )
                })
            })
        )
    }

    public getProject(projectRef: string) : Observable<IProject> {
        return this.afs.doc<IProject>("project/" + projectRef).snapshotChanges().pipe(
            map(projectDocumentSnapshotAction => {
                const projectRef = projectDocumentSnapshotAction.payload.id;
                const projectData = projectDocumentSnapshotAction.payload.data();
                return new Project (
                    projectRef,
                    projectData.name,
                    projectData.taskManagerRef,
                    projectData.poleRef,
                    projectData.userRefs
                )
            })
        );
    }
}
