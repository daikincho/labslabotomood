export enum ESmileyMark {
    VERRY_UNHAPPY,
    QUITE_UNHAPPY,
    QUITE_HAPPY,
    VERRY_HAPPY,
}

export interface ISmileyMark {
    name?: string;
}

export class SmileyMark implements ISmileyMark {
    constructor (
        public name?: string
    ) { }
}
