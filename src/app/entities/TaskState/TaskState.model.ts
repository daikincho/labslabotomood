export interface ITaskState {
    state?: string;
}

export class TaskState {
    constructor (
        public state?: string
    ) { }
}
