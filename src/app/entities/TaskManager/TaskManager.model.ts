import { ITaskList, TaskList } from "../TaskList/TaskList.model";
import { DocumentReference } from "@angular/fire/firestore";
import { ITask } from "../Task/Task.model";

export interface ITaskManager {
    ref: string;
    name?: string;
    taskListRefs?: DocumentReference[];
}

export class TaskManager implements ITaskManager {

    public taskLists: TaskList[] = [];
    public lastTask: ITask;

    constructor (
        public ref: string,
        public name?: string,
        public taskListRefs?: DocumentReference[]
    ) { 
        this.initTaskLists();
    }

    private initTaskLists() {
        for (let taskListRef of this.taskListRefs) {
            taskListRef.get().then((taskListDoc) => {
                const taskListData = taskListDoc.data()
                this.taskLists.push(new TaskList(
                    taskListData.name,
                    taskListData.taskRefs
                ));
            })
        }
    }
}
