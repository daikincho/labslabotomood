import { ITaskState, TaskState } from "../TaskState/TaskState.model";
import { DocumentReference } from "@angular/fire/firestore";

export interface ITask {
    ref?: string;
    title?: string;
    description?: string;
    date?: Date;
    taskStateRef?: DocumentReference;
}

export class Task {

    public tastState: TaskState;

    constructor (
        public ref?: string,
        public title?: string,
        public description?: string,
        public date?: Date,
        public taskStateRef?: DocumentReference
    ) {
        this.initTaskState();
    }

    private initTaskState() {
        this.taskStateRef.get().then((taskStateDoc) => {
            let taskStateData = taskStateDoc.data();
            this.tastState = new TaskState(
                taskStateData.name
            )
        })
    }
}
