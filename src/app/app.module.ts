import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { environment } from './../environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';


import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirebaseDatabaseService } from '../Services/FirebaseDatabaseService';
import { IonicStorageModule } from '@ionic/storage';
import { PolesFirebaseService } from '../Services/PolesFirebaseService';
import { TasksFirebaseService } from '../Services/TasksFirebaseService';


import { LocalNotifications } from "@ionic-native/local-notifications/ngx";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFireModule.initializeApp(environment.firebase),IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    AngularFirestore,
    SplashScreen,
    AngularFirestore,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy, },
    LocalNotifications,
    FirebaseDatabaseService,
    PolesFirebaseService,
    TasksFirebaseService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
