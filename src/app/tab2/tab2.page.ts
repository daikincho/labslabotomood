import { Component } from '@angular/core';
import { Project } from '../entities/Project/Project.model';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators'
import lodash from 'lodash';
import { FirebaseDatabaseService } from '../../Services/FirebaseDatabaseService';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public allProjects : any[] = [];
  myProjects : any[] = [];
  
  currentUser : any;
  projectDocCollection:any;
  usersDocCollection : any;

  constructor(
    private _database : AngularFirestore,
    private storage: Storage,
    public firebaseDatabaseService : FirebaseDatabaseService
  ){
    this.projectDocCollection = _database.collection<any>('Project');
    this.usersDocCollection = _database.collection<any>('users');

  }

  ngOnInit(){

    this.firebaseDatabaseService.InitiliseDatabase(this);

  }

  




  Save(){
    this.firebaseDatabaseService.Save(this);
  }

  

  onProjectStatusChanged(newValue, project){

  }


}
