import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicStorageModule } from '@ionic/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    AngularFirestoreModule,
    IonicStorageModule.forRoot(),
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab2Page }]),
    AngularFireAuthModule
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
