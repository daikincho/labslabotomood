import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators'
import lodash from 'lodash';
import { Injectable } from '@angular/core';
import { Tab2Page } from '../app/tab2/tab2.page';


@Injectable()
export class TasksFirebaseService { 


  constructor(
    private _database : AngularFirestore){


  }
  
  public InitiliseDatabase(tab) {

    
        tab.tasksDocCollection = this._database.collection<any>('tasks');

        this._database.collection<any>('tasks').valueChanges().subscribe(data => {
        
        tab.allTasks = data;

        tab.storage.get('UserConnected').then((user) => {
            tab.currentUser = user;
          
          });

      });

      this._database.collection<any>('Project').valueChanges().subscribe(data => {
        tab.projectDocCollection = data;
      });

      this._database.collection<any>('Project').valueChanges().pipe(
        map((projects) => {
          return projects.map((x) => {
            return {
              'id': x.id,
              'name': x.Name,
              'pole' : x.pole,
              'userIds' : x['users_ref'] ? x['users_ref'].map(y => y['id']) : []
            }
          })
        })
      ).subscribe(data => {

        tab.allProjects = data;

        
        tab.storage.get('UserConnected').then((user) => {
          tab.currentUser = user;

          tab.allProjects.filter( p => {
  
            if(p.userIds){
  
              if(p.userIds.includes(tab.currentUser.id)){
                p.follow = true;
              }else{
                p.follow = false;
              }
  
              return p;
  
            }else{
              p.follow = false;
              return false;
            }
            
          });


          tab.allTasks.filter( task => {
            task.project = "";
            
            tab.allProjects.filter(project => {
                
                if(task.Project_ref_s == project.id){
                  if(project.userIds.includes(tab.currentUser.id)){
                    task.project = project;
                  }
                    
                }
            });
        });

        
        });


        
  
  
      });



  }

  saveNewTask(tab){

    let myTask = {
      title : tab.TaskTitle,
      description : tab.TaskDescription,
      Project_ref_s: tab.TaskProject.id,
      id:"",
      users_ref_s:[]

    }

    tab.tasksDocCollection.add(myTask).then(docRef => {
      myTask.id = docRef.id;
      let taskDoc = this._database.doc<any>(`tasks/${myTask.id}`); 
      taskDoc.update(myTask);

    });

    




  }





  
}