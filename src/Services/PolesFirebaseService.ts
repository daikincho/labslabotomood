import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators'
import { fileURLToPath } from 'url';









@Injectable()
export class PolesFirebaseService { 


  constructor(
    private _database : AngularFirestore){


  }


  InitiliseDatabase(tab) {


    this._database.collection<any>('poles').valueChanges().subscribe(data => {
        tab.polesDocCollection = data;
        tab.allPoles = data;

        tab.storage.get('UserConnected').then((user) => {
            tab.currentUser = user;
          
          });

      });
  
      this._database.collection<any>('Project').valueChanges().subscribe(data => {
        tab.projectDocCollection = data;
      });

      this._database.collection<any>('Project').valueChanges().pipe(
        map((projects) => {
          return projects.map((x) => {
            return {
              'id': x.id,
              'name': x.Name,
              'pole' : x.pole,
              'userIds' : x['users_ref'] ? x['users_ref'].map(y => y['id']) : []
            }
          })
        })
      ).subscribe(data => {
        
        tab.allProjects = data;

        tab.allPoles.filter( pole => {
            pole.projects = [];
            
            tab.allProjects.filter(project => {
                
                if(pole.name == project.pole){
                    pole.projects.push(project.name)
                }
            });
        });

        
        tab.storage.get('UserConnected').then((user) => {
          tab.currentUser = user;

          tab.allProjects.filter( p => {
  
            if(p.userIds){
  
              if(p.userIds.includes(tab.currentUser.id)){
                p.follow = true;
              }else{
                p.follow = false;
              }
  
              return p;
  
            }else{
              p.follow = false;
              return false;
            }
            
          });
        
        });
  
  
      });


  
      


  }






}