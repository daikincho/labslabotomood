import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators'
import lodash from 'lodash';
import { Injectable } from '@angular/core';
import { Tab2Page } from '../app/tab2/tab2.page';


@Injectable()
export class FirebaseDatabaseService { 


  constructor(
    private _database : AngularFirestore){


  }
  
  public InitiliseDatabase(tab) {


    this._database.collection<any>('users').valueChanges().subscribe(data => {
        tab.usersDocCollection = data;
      });
  
      this._database.collection<any>('Project').valueChanges().subscribe(data => {
        tab.projectDocCollection = data;
      });
  
      this._database.collection<any>('Project').valueChanges().pipe(
        map((projects) => {
          return projects.map((x) => {
            return {
              'id': x.id,
              'name': x.Name,
              'userIds' : x['users_ref'] ? x['users_ref'].map(y => y['id']) : []
            }
          })
        })
      ).subscribe(data => {
        //console.log(data);
        //lodash simplifie le json de users_ref qui s'avere etre trop lourd
        tab.allProjects = data;
  
        tab.storage.get('UserConnected').then((user) => {
          tab.currentUser = user;
          // myProjects = allProjects.find(p => p.userIds.includes(currentUser.id))
          tab.myProjects = tab.allProjects.filter( p => {
  
            if(p.userIds){
  
              if(p.userIds.includes(tab.currentUser.id)){
                p.follow = true;
              }else{
                p.follow = false;
              }
  
              return p;
  
            }else{
              p.follow = false;
              return false;
            }
            
            //return p.userIds ? p.userIds.includes(currentUser.id) : false;
          });
  
          tab.myProjects.filter(p => {
  
            p.follow = tab.allProjects.find(all_p => all_p.id == p.id ).follow;
  
          } );
    
          console.log(tab.myProjects);
    
        });
  
  
      });


  }

  Save(tab){

    let userDoc = this._database.doc<any>('users/'+tab.currentUser.id);
   // let myAllProjectsRef = this.projectDocCollection.filter(p => )
    let all_local_CurrentUsers_ref = [];

    tab.myProjects = tab.allProjects.filter(aP => aP.follow == true);
    let notMyProjects = tab.allProjects.filter(aP => aP.follow == false);



    tab.myProjects.map((p) => {
      //let projectDoc = this._database.doc<any>('Project/'+p.id);

      tab.projectDocCollection.filter( pO => {
        if(pO.Name == p.name){
          all_local_CurrentUsers_ref = pO.users_ref;
        }
      
      }
      
      );
      
      let projectDoc = this._database.doc<any>(`Project/${p.id}`);

      let allUserIds = [];
      all_local_CurrentUsers_ref.filter(ref => {

        if(ref != null){
          allUserIds.push(ref.id);
        }


      });

      if(p.id == projectDoc.ref.id){//si c'est le meme projet
        if(!(allUserIds.includes(tab.currentUser.id))){//si l'id exist pas dans la bdd
          all_local_CurrentUsers_ref.push(userDoc.ref);
        }

        projectDoc.update({
          users_ref : all_local_CurrentUsers_ref
        });

      }
      
    });


    notMyProjects.map((p) => {
      //let projectDoc = this._database.doc<any>('Project/'+p.id);

      tab.projectDocCollection.filter( pO => {
        if(pO.Name == p.name){
          all_local_CurrentUsers_ref = pO.users_ref;
        }
      
      }
      
      );
      
      let projectDoc = this._database.doc<any>(`Project/${p.id}`);

      let allUserIds = [];
      all_local_CurrentUsers_ref.filter(ref => {

        if(ref != null){
          allUserIds.push(ref.id);
        }


      });

      if(p.id == projectDoc.ref.id){//si c'est le meme projet
        if((allUserIds.includes(tab.currentUser.id))){//si l'id exist dans la bdd

          let index = all_local_CurrentUsers_ref.findIndex(ref=>{return ref.id == tab.currentUser.id},1);
          all_local_CurrentUsers_ref.splice(index,1);
        }

        projectDoc.update({
          users_ref : all_local_CurrentUsers_ref
        });

      }
      
    });


    //user Project_ref maj--------------------------

    let allMyProjectIds = [];
    let all_local_CurrentProject_ref = [];
    let projectDoc;

    tab.usersDocCollection.filter( uO => {
      if(uO.email == tab.currentUser.email){
        all_local_CurrentProject_ref = uO.Project_ref;
      }
    
    }
    
    );

    all_local_CurrentProject_ref.filter(ref => {

      if(ref != null){
        allMyProjectIds.push(ref.id);
      }


    });


    let myNewProjectsref = [];

    tab.myProjects.filter(p => {
      if(p != null){
        projectDoc = this._database.doc<any>('Project/'+p.id);
        myNewProjectsref.push(projectDoc.ref);
      }
    });

    userDoc.update({
      Project_ref : myNewProjectsref
    });



    for(let i; i < tab.myProjects.length -1; i++){

    }

    tab.myProjects.forEach(x => {

    })


  }



  
}