var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProjectService } from '../entities/Project/Project.service';
var TaskTrackingPage = /** @class */ (function () {
    function TaskTrackingPage(activateRoute) {
        this.activateRoute = activateRoute;
        this.projectNotFound = false;
    }
    TaskTrackingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.activateRoute.paramMap.subscribe(function (params) {
            ProjectService.get(+params.get("projectId")).subscribe(function (project) {
                _this.project = project;
            }, function (error) {
                _this.projectNotFound = true;
            });
        });
    };
    TaskTrackingPage = __decorate([
        Component({
            selector: 'app-task-tracking',
            templateUrl: './task-tracking.page.html',
            styleUrls: ['./task-tracking.page.scss'],
        }),
        __metadata("design:paramtypes", [ActivatedRoute])
    ], TaskTrackingPage);
    return TaskTrackingPage;
}());
export { TaskTrackingPage };
//# sourceMappingURL=task-tracking.page.js.map