var Pole = /** @class */ (function () {
    function Pole(id, name, projects) {
        this.id = id;
        this.name = name;
        this.projects = projects;
    }
    return Pole;
}());
export { Pole };
//# sourceMappingURL=Pole.model.js.map