var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pole } from './Pole.model';
import { Project } from '../Project/Project.model';
var PoleService = /** @class */ (function () {
    function PoleService() {
    }
    PoleService.getAll = function () {
        return new Observable(function (observer) {
            var result = [
                new Pole(1, "Game", [
                    new Project(1, "Pokemon Runeterra Edition 3D (best game ever btw)", null, null),
                    new Project(1, "Un autre projet", null, null)
                ])
            ];
            observer.next(result);
            observer.complete();
            return;
            observer.error("Not found");
        });
        ;
    };
    PoleService = __decorate([
        Injectable({ providedIn: 'root' })
    ], PoleService);
    return PoleService;
}());
export { PoleService };
//# sourceMappingURL=Pole.service.js.map