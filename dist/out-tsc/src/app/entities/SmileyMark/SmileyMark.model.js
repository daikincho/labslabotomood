export var ESmileyMark;
(function (ESmileyMark) {
    ESmileyMark[ESmileyMark["VERRY_UNHAPPY"] = 0] = "VERRY_UNHAPPY";
    ESmileyMark[ESmileyMark["QUITE_UNHAPPY"] = 1] = "QUITE_UNHAPPY";
    ESmileyMark[ESmileyMark["QUITE_HAPPY"] = 2] = "QUITE_HAPPY";
    ESmileyMark[ESmileyMark["VERRY_HAPPY"] = 3] = "VERRY_HAPPY";
})(ESmileyMark || (ESmileyMark = {}));
var SmileyMark = /** @class */ (function () {
    function SmileyMark(name) {
        this.name = name;
    }
    return SmileyMark;
}());
export { SmileyMark };
//# sourceMappingURL=SmileyMark.model.js.map