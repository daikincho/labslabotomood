var Session = /** @class */ (function () {
    function Session(date, opinions) {
        this.date = date;
        this.opinions = opinions;
    }
    return Session;
}());
export { Session };
//# sourceMappingURL=Session.model.js.map