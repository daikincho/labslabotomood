var Opinion = /** @class */ (function () {
    function Opinion(accomplishedTasks, difficultiesMet, smileyMark, session, user) {
        this.accomplishedTasks = accomplishedTasks;
        this.difficultiesMet = difficultiesMet;
        this.smileyMark = smileyMark;
        this.session = session;
        this.user = user;
    }
    return Opinion;
}());
export { Opinion };
//# sourceMappingURL=Opinion.model.js.map