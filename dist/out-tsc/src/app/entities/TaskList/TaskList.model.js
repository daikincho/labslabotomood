var TaskList = /** @class */ (function () {
    function TaskList(id, name, tasks) {
        this.id = id;
        this.name = name;
        this.tasks = tasks;
    }
    return TaskList;
}());
export { TaskList };
//# sourceMappingURL=TaskList.model.js.map