var Project = /** @class */ (function () {
    function Project(id, name, taskManager, pole, users) {
        this.id = id;
        this.name = name;
        this.taskManager = taskManager;
        this.pole = pole;
        this.users = users;
    }
    return Project;
}());
export { Project };
//# sourceMappingURL=Project.model.js.map