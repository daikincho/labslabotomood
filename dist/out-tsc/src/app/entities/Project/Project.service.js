var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from './Project.model';
import { TaskManager } from '../TaskManager/TaskManager.model';
import { TaskList } from '../TaskList/TaskList.model';
import { Task } from '../Task/Task.model';
import { TaskState } from '../TaskState/TaskState.model';
var ProjectService = /** @class */ (function () {
    function ProjectService() {
    }
    ProjectService.get = function (id) {
        return new Observable(function (observer) {
            var projects = [
                new Project(1, "Pokemon Runeterra Edition 3D (best game ever btw)", new TaskManager(1, "Work work work", [
                    new TaskList(1, "Battle part", [
                        new Task(1, "Battle encounter animation", new TaskState("PENDING")),
                        new Task(2, "Battle structure", new TaskState("ACCEPTED"))
                    ]),
                    new TaskList(1, "Overworld part", [
                        new Task(1, "Review movement manager structure", new TaskState("ACCEPTED")),
                        new Task(2, "Documentation", new TaskState("ACCEPTED"))
                    ])
                ]), null),
                new Project(2, "Un autre jeu", null, null)
            ];
            if (projects[id]) {
                observer.next(projects[id]);
                observer.complete();
                return;
            }
            observer.error("Not found");
        });
    };
    ProjectService = __decorate([
        Injectable({ providedIn: 'root' })
    ], ProjectService);
    return ProjectService;
}());
export { ProjectService };
//# sourceMappingURL=Project.service.js.map