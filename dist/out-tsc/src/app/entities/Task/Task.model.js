var Task = /** @class */ (function () {
    function Task(id, description, taskState) {
        this.id = id;
        this.description = description;
        this.taskState = taskState;
    }
    return Task;
}());
export { Task };
//# sourceMappingURL=Task.model.js.map