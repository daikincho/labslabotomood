var User = /** @class */ (function () {
    function User(email, name, roles, project, pole) {
        this.email = email;
        this.name = name;
        this.roles = roles;
        this.project = project;
        this.pole = pole;
    }
    return User;
}());
export { User };
//# sourceMappingURL=User.model.js.map