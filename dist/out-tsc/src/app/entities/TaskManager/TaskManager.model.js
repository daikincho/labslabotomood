var TaskManager = /** @class */ (function () {
    function TaskManager(id, name, taskLists) {
        this.id = id;
        this.name = name;
        this.taskLists = taskLists;
    }
    return TaskManager;
}());
export { TaskManager };
//# sourceMappingURL=TaskManager.model.js.map