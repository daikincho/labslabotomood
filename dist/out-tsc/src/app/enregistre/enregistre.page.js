var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { firebase } from '@firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { userLabo } from '../../models/userLabo';
import '@firebase/auth';
import '@firebase/database';
import { Router } from '@angular/router';
var EnregistrePage = /** @class */ (function () {
    function EnregistrePage(_toastCtrl, _auth, _database, router) {
        this._toastCtrl = _toastCtrl;
        this._auth = _auth;
        this._database = _database;
        this.router = router;
        this.currentUser = new userLabo();
        this.userDocCollection = _database.collection('users');
    }
    EnregistrePage.prototype.ngOnInit = function () {
    };
    EnregistrePage.prototype.signupUser = function () {
        var _this = this;
        if (this.currentUser.password == this.confirmation_password
            && this.currentUser.roles.length > 0
            && this.currentUser.roles.length < 3) {
            return firebase
                .auth()
                .createUserWithEmailAndPassword(this.currentUser.email, this.currentUser.password)
                .then(function (newUserCredential) {
            })
                .then(function (_) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this._toastCtrl.create({
                                message: 'you are now created !',
                                color: 'success',
                                duration: 3000
                            })];
                        case 1:
                            (_a.sent()).present();
                            return [2 /*return*/];
                    }
                });
            }); }).then(function (success) {
                _this.CreateUserInDataBase();
                _this.router.navigate(['/tabs']);
            })
                .catch(function (err) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this._toastCtrl.create({
                                message: 'error: ' + err,
                                color: 'danger',
                                duration: 2000
                            })];
                        case 1:
                            (_a.sent()).present();
                            return [2 /*return*/];
                    }
                });
            }); });
        }
        this.presentToast('you are not created ! ', 'danger');
        return null;
    };
    EnregistrePage.prototype.presentToast = function (message, color) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._toastCtrl.create({
                            message: message,
                            duration: 2000,
                            color: color
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EnregistrePage.prototype.CreateUserInDataBase = function () {
        this.userDocCollection.add({
            name: this.currentUser.name,
            email: this.currentUser.email,
            roles: this.currentUser.roles,
            password: this.currentUser.password,
            poles: this.currentUser.poles
            // Other info you want to add here
        });
        /*this._database.collection<userLabo>('users').valueChanges().subscribe(data=>{
          this.userDoc = this._database.doc<any>('users/7rZqq8DZOvjWDDThS136');
    
          this.userDoc.set({
            name: this.currentUser.name,
            email: this.currentUser.email,
            // Other info you want to add here
          })*/
    };
    EnregistrePage = __decorate([
        Component({
            selector: 'app-enregistre',
            templateUrl: './enregistre.page.html',
            styleUrls: ['./enregistre.page.scss'],
        }),
        __metadata("design:paramtypes", [ToastController, AngularFireAuth, AngularFirestore, Router])
    ], EnregistrePage);
    return EnregistrePage;
}());
export { EnregistrePage };
//# sourceMappingURL=enregistre.page.js.map