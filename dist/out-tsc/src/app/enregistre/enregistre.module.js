var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicModule } from '@ionic/angular';
import { EnregistrePage } from './enregistre.page';
var routes = [
    {
        path: '',
        component: EnregistrePage
    }
];
var EnregistrePageModule = /** @class */ (function () {
    function EnregistrePageModule() {
    }
    EnregistrePageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                AngularFirestoreModule,
                RouterModule.forChild(routes),
                AngularFireAuthModule
            ],
            declarations: [EnregistrePage]
        })
    ], EnregistrePageModule);
    return EnregistrePageModule;
}());
export { EnregistrePageModule };
//# sourceMappingURL=enregistre.module.js.map