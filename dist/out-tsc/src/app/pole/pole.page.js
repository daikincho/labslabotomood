var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { PoleService } from '../entities/Pole/Pole.service';
var PolePage = /** @class */ (function () {
    function PolePage() {
        this.errorOccured = false;
    }
    PolePage.prototype.ngOnInit = function () {
        var _this = this;
        PoleService.getAll().subscribe(function (poles) {
            _this.poles = poles;
        }, function (error) {
            _this.errorOccured = true;
        });
    };
    PolePage = __decorate([
        Component({
            selector: 'app-pole',
            templateUrl: 'pole.page.html',
            styleUrls: ['pole.page.scss']
        }),
        __metadata("design:paramtypes", [])
    ], PolePage);
    return PolePage;
}());
export { PolePage };
//# sourceMappingURL=pole.page.js.map