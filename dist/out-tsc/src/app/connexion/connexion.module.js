var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicStorageModule } from '@ionic/storage';
import { IonicModule } from '@ionic/angular';
import { ConnexionPage } from './connexion.page';
var routes = [
    {
        path: '',
        component: ConnexionPage
    }
];
var ConnexionPageModule = /** @class */ (function () {
    function ConnexionPageModule() {
    }
    ConnexionPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                IonicStorageModule.forRoot(),
                AngularFirestoreModule,
                RouterModule.forChild(routes),
                AngularFireAuthModule
            ],
            declarations: [ConnexionPage]
        })
    ], ConnexionPageModule);
    return ConnexionPageModule;
}());
export { ConnexionPageModule };
//# sourceMappingURL=connexion.module.js.map